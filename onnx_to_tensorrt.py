import numpy as np
import cv2, sys, os
import numpy as np
import tensorrt as trt
import pycuda.driver as cuda
import pycuda.autoinit
from common import preprocess_image, read_txt, softmax, cmd

ONNX_PATH = "model/pytorch_renset18_batch1.onnx"
SAVE_PATH = "model/pytorch_renset18_batch1.trt"
USE_FP16 = True
TARGET_DTPYE = np.float16 if USE_FP16 else np.float32
BATCH_SIZE=1
SHAPE = [224, 224, 3]
IMAGE_PATH = "data/cat.jpg"
CLASS_PATH = "data/imagenet_classes.txt"

def convert_tensorrt():
    # Step out of Python for a moment to convert the ONNX model to a TRT engine using trtexec
    if USE_FP16:
        command = f"trtexec --onnx={ONNX_PATH} \
                    --saveEngine={SAVE_PATH} \
                    --explicitBatch --inputIOFormats=fp16:chw \
                    --outputIOFormats=fp16:chw --fp16"
        cmd(command)
    else:
        command = f"trtexec --onnx={ONNX_PATH} \
                    --saveEngine={SAVE_PATH} \
                    --explicitBatch"
        cmd(command)

def load_model(input_batch):
    f = open(SAVE_PATH, "rb")
    runtime = trt.Runtime(trt.Logger(trt.Logger.WARNING)) 
    engine = runtime.deserialize_cuda_engine(f.read())
    context = engine.create_execution_context()
    
    # Need to set input and output precisions to FP16 to fully enable it
    output = np.empty([BATCH_SIZE, 1000], dtype = TARGET_DTPYE) 
    # Allocate device memory
    d_input = cuda.mem_alloc(1 * input_batch.nbytes)
    d_output = cuda.mem_alloc(1 * output.nbytes)
    bindings = [int(d_input), int(d_output)]
    stream = cuda.Stream()
    
    return context, d_input, bindings, stream, output, d_output

def inference(preprocessed_images, input_batch):
    context, d_input, bindings, stream, output, d_output = load_model(input_batch)
    
    # transfer input data to device
    cuda.memcpy_htod_async(d_input, preprocessed_images, stream)
    # execute model
    context.execute_async_v2(bindings, stream.handle, None)
    # transfer predictions back
    cuda.memcpy_dtoh_async(output, d_output, stream)
    # syncronize threads
    stream.synchronize()
    pred = output.squeeze()
    pred = softmax(pred)
    
    return pred

def main():
    # Export tensorrt model
    if not os.path.exists(SAVE_PATH):
        convert_tensorrt()
    
    # Loading images
    img = cv2.imread(IMAGE_PATH)
    input_batch = np.array(np.repeat(np.expand_dims(np.array(img, dtype=np.float32), axis=0), BATCH_SIZE, axis=0), dtype=np.float16)
    pre_image = preprocess_image(img, SHAPE)
    pre_image = np.array(pre_image, dtype=np.float16)

    # Inference
    pred = inference(pre_image, input_batch)

    # Loading classes.txt
    classes = read_txt(CLASS_PATH)
    classes = classes.split("\n")   
    
    # Results
    indices = (-pred).argsort()[:5]
    classes_list = [classes[int(idx)] for idx in indices]
    print(list(zip(classes_list, pred[indices])))
    
if __name__ == "__main__":
    sys.exit(main() or 0)
