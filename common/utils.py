
import torch, cv2, subprocess
import numpy as np
from torchvision.transforms import Normalize

def read_txt(path:str):
    with open(path) as f:
        return f.read()

def softmax(x):
    x -= np.max(x)
    exp_x = np.exp(x)
    return exp_x / exp_x.sum(axis=0)

def preprocess_image(img, SHAPE):
    img = cv2.resize(img, (SHAPE[1], SHAPE[0]), cv2.INTER_LINEAR) / 255
    # img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
    norm = Normalize(mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225])
    result = norm(torch.from_numpy(img).permute(2, 0, 1))
    result = np.expand_dims(result, axis=0).astype(np.float32)
    result = np.ascontiguousarray(result)  # 强制变为连续数组
    return result

def cmd(command):
    process = subprocess.Popen(command.split(), stdout=subprocess.PIPE, stderr=subprocess.STDOUT, shell=False)
    for line in iter(process.stdout.readline,b''):
        line = line.rstrip().decode()
        if line.isspace(): 
            continue
        else:
            print(line)
