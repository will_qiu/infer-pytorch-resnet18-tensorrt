import torch, cv2, sys, os
import numpy as np
import onnxruntime as rt
import torch.onnx
import torchvision.models as models
from common import preprocess_image, read_txt, softmax

BATCH_SIZE=1
SHAPE = [224, 224, 3]
MODEL_SAVE_PATH = "model/pytorch_renset18_batch1.onnx"
IMAGE_PATH = "data/cat.jpg"
CLASS_PATH = "data/imagenet_classes.txt"

def load_model():
    providers = [
        ('CUDAExecutionProvider', {
            'device_id': 0,
            'arena_extend_strategy': 'kNextPowerOfTwo',
            'gpu_mem_limit': 4 * 1024 * 1024 * 1024,
            'cudnn_conv_algo_search': 'EXHAUSTIVE',
            'do_copy_in_default_stream': True,
        }),
        'CPUExecutionProvider',
    ]
    sess = rt.InferenceSession(MODEL_SAVE_PATH, providers=providers)
    sess_input = sess.get_inputs()[0].name
    sess_output = sess.get_outputs()[0].name
    # Warm UP
    print("Warmup!!!")
    input_data = np.random.rand(1,3,224,224).astype(np.float32)
    num_warmup = 10
    for i in range(num_warmup):
        sess.run(None, {sess_input: input_data})
    print("Warmup end!!!")
    
    return sess, sess_input, sess_output

def inference(preprocessed_images):
    sess, sess_input, sess_output = load_model()
    onnx_result = sess.run([sess_output], {sess_input: preprocessed_images})[0]
    pred = onnx_result.squeeze()
    pred = softmax(pred)
    return pred
    
def export_onnx_model():
    # Loading the pretrained model
    resnet18 = models.resnet18(pretrained=True, progress=False).eval()
    dummy_input=torch.randn(BATCH_SIZE, SHAPE[2], SHAPE[1], SHAPE[0])
    # Export the model to ONNX
    torch.onnx.export(resnet18, dummy_input, MODEL_SAVE_PATH, verbose=False)

def main():
    # Export onnx model
    if not os.path.exists(MODEL_SAVE_PATH):
        export_onnx_model()
    
    # Loading images
    img = cv2.imread(IMAGE_PATH)
    pre_image = preprocess_image(img, SHAPE) 
    
    # Inference
    pred = inference(pre_image)
    
    # Loading classes.txt
    classes = read_txt(CLASS_PATH)
    classes = classes.split("\n")   
    
    # Results
    indices = (-pred).argsort()[:5]
    classes_list = [classes[int(idx)] for idx in indices]
    print(list(zip(classes_list, pred[indices])))
    
if __name__ == "__main__":
    sys.exit(main() or 0)
