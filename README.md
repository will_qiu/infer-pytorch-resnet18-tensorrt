# Pytorch resnet18 convert to tensorrt and inference
This is convert pytorch-resnet18 to onnx and tensorrt code and inference it will getting result.

## Getting Started

## Ubuntu
### Pre-requirements

### Docker
Install **nvidia-driver**, **nvidia-docker** and **docker** before installing the docker container.

- [Tutorial-nvidia-driver](https://docs.nvidia.com/datacenter/tesla/tesla-installation-notes/index.html)

- [Tutorial-docker](https://docs.docker.com/engine/install/ubuntu/)

- [Tutorial-nvidia-docker](https://docs.nvidia.com/datacenter/cloud-native/container-toolkit/install-guide.html#docker)

- **Add docker to sudo group** 
    - [Tutourial](https://docs.docker.com/engine/install/linux-postinstall/)
    ``` 
    sudo groupadd docker
    sudo usermod -aG docker $USER
    sudo chmod 777 /var/run/docker.sock
    ```
- Build docker
    ```shell
    sudo chmod u+x ./docker/*.sh
    ./docker/build.sh
    ```
- Run container
    ```shell
    sudo chmod u+x ./docker/*.sh
    xhost +
    ./docker/run.sh
    ```
### Run code
- **Convert Pytorch-resnet18 to Onnx** 
    ```shell
    python3 pytorch-resnet_to_onnx.py
    ```

- **Convert Onnx-resnet18 to TensorRT** 
    ```shell
    python3 onnx_to_tensorrt.py
    ```
    <details>
        <summary> Result
        </summary>
        <div align="center">
            <img width="80%" height="80%" src="./docs/command.png">
        </div>
    </details>