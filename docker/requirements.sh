#!/bin/bash
# ---------------------------------------------------------
# Color ANIS
RED='\033[1;31m'
BLUE='\033[1;34m'
YELLOW='\033[1;33m'
GREEN='\033[1;32m'
NC='\033[0m'

# ---------------------------------------------------------
function printstr(){
    echo -e "${BLUE}"
    echo $1
    echo -e "${NC}"
}

# ---------------------------------------------------------
# OpenCV
printstr "$(date +"%T") Install OpenCV " 
apt-get update && apt-get install -qqy ffmpeg libsm6 libxext6
apt-get install -qqy libxrender-dev
pip install opencv-python==4.5.5.64 
pip install "opencv-python-headless<4.3"

# Jupyter notebook / onnx
printstr "$(date +"%T") Install Onnx / Onnxruntime " 
pip install onnx onnxruntime-gpu

# Pycuda / tensorRT
printstr "$(date +"%T") Install pycuda / tensorRT "
export PATH=/usr/local/cuda/bin:$PATH
export CUDA_ROOT=/usr/local/cuda
pip install --upgrade setuptools wheel
pip install --upgrade pip
apt update
apt install -qqy python3-pycuda
apt update
apt install -qqy build-essential python3-dev
pip install pycuda==2022.1